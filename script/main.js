"use strict";
// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
// як я це зрозумів то це внутрішня будова ШТМЛ сторінки в об'єктному вигляді,вона дає змогу скрипту отримати доступ до ШТМЛ сторінки, також дає змогу змінювати зміст, змінювати структуру / вигляд сторінки при тих чи інших діях користувача
// Какая разница между свойствами HTML-элементов innerHTML и innerText?
// innerHTML це взагалі весь тег, з його текстом , вбудованими тегами між головним тегом на початку і в кінці наприклад blabla<span>blabla</span>, а innerText це лише текст , у попередньому випадку вивело б нам "blablablabla";
// Как можно обратится к элементу страницы с помощью JS? Какой способ предпочтительнее?
// document.getElement* та document.querySelector,querySelectorAll, більш нові методи це query вони зручніші , нам не потрібно шукати по айді або класу окремими методами, лише правильний синтаксис і можна знайти все це за допомогою querySelector, також метод get повертає нам живу колекцію, а querySelector повертає статичну.

let paragraf = document.querySelectorAll("p");
for(let elements of paragraf){
        elements.style.backgroundColor="#ff0000";
};
let optionsList = document.querySelector("#optionsList");
console.log(optionsList);
console.log(optionsList.parentNode);
for (let element of optionsList.childNodes){
    console.log(element.nodeName ,element.nodeType);
};
let testParagraph = document.querySelector("#testParagraph");
console.log(testParagraph.innerHTML);
testParagraph.innerHTML = `<p>This is a paragraph</p>`;
console.log(testParagraph.innerHTML);
let mainHeaderLi = document.querySelectorAll(" .main-header li");
console.log(mainHeaderLi);
for(let li of mainHeaderLi){
    li.classList.add("nav-item");
};
let sectionTitle = document.querySelectorAll(".section-title");
for (let element of sectionTitle){
    element.classList.toggle("section-title");
}
console.log(sectionTitle);